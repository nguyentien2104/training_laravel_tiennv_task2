<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\DiscountController;
use App\Http\Controllers\API\DishController;
use App\Http\Controllers\API\ManagementAreaController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\StoreController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::get('/store/list', [DishController::class, 'listDishByStore']);

Route::group([
    'middleware' => 'admin',
    'prefix' => 'admin'
], function () {
    Route::group([
        'prefix' => 'users'
    ], function () {
        Route::get('/', [UserController::class, 'list']);
        Route::get('/detail/{id}', [UserController::class, 'detail']);
        Route::post('/create', [UserController::class, 'create']);
        Route::put('/update/{id}', [UserController::class, 'update']);
        Route::delete('/delete/{id}', [UserController::class, 'delete']);
    });
    Route::group([
        'prefix' => 'areas'
    ], function () {
        Route::get('/', [ManagementAreaController::class, 'list']);
        Route::get('/detail/{id}', [ManagementAreaController::class, 'detail']);
        Route::post('/create', [ManagementAreaController::class, 'create']);
        Route::put('/update/{id}', [ManagementAreaController::class, 'update']);
        Route::delete('/delete/{id}', [ManagementAreaController::class, 'delete']);
    });

    Route::group([
        'prefix' => 'discount-code'
    ], function () {
        Route::get('/list', [DiscountController::class, 'list']);
        Route::get('/detail/{id}', [DiscountController::class, 'detail']);
        Route::post('/create', [DiscountController::class, 'create']);
        Route::put('/update/{id}', [DiscountController::class, 'update']);
        Route::delete('/delete/{id}', [DiscountController::class, 'delete']);
    });

    Route::group([
        'prefix' => 'store'
    ], function () {
        Route::get('/list', [StoreController::class, 'list']);
        Route::get('/detail/{id}', [StoreController::class, 'detail']);
        Route::post('/create', [StoreController::class, 'create']);
        Route::put('/update/{id}', [StoreController::class, 'update']);
        Route::delete('/delete/{id}', [StoreController::class, 'delete']);
    });

    Route::group([
        'prefix' => 'dish'
    ], function () {
        Route::get('/list', [DishController::class, 'list']);
        Route::get('/detail/{id}', [DishController::class, 'detail']);
        Route::post('/create', [DishController::class, 'create']);
        Route::put('/update/{id}', [DishController::class, 'update']);
        Route::delete('/delete/{id}', [DishController::class, 'delete']);
    });
});

Route::group([
    'prefix' => 'users',
    'middleware' => 'auth-api'
], function () {
    Route::put('/update-profile', [AuthController::class, 'updateProfile']);
    Route::get('get-profile', [AuthController::class, 'getProfile']);
    Route::post('/logout', [AuthController::class, 'logout']);
});

Route::group([
    'middleware' => 'user'
], function () {
    Route::get('/transfer-money', [UserController::class, 'rechargeWallet']);
    Route::post('/order', [OrderController::class, 'order']);


});

