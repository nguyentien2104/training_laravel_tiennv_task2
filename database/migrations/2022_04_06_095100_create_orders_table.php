<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->tinyInteger('order_status')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->integer('ship_fee');
            $table->boolean('paid_status');
            $table->unsignedBigInteger('discount_code_id');
            $table->text('note');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('discount_code_id')->references('id')->on('discount_codes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
