<?php

namespace Database\Seeders;

use App\Enums\UserRole;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();

        $faker = \Faker\Factory::create();
        $password = Hash::make(config('setting.password_default'));
        $isAdmin = UserRole::ADMINISTRATOR;

        User::create([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'address' => $faker->address(),
            'phone' => $faker->phoneNumber(),
            'password' => $password,
            'wallet' => 0,
            'is_admin' =>  $isAdmin,
        ]);


        for ($i = 0; $i < 5; $i++) {
            User::create([
                'name' => $faker->name(),
                'email' => $faker->email(),
                'address' => $faker->address(),
                'phone' => $faker->phoneNumber(),
                'password' => $password,
                'wallet' => 0,
            ]);
        }
        for ($i = 0; $i < 5; $i++) {
            User::create([
                'name' => $faker->name(),
                'email' => $faker->email(),
                'address' => $faker->address(),
                'phone' => $faker->phoneNumber(),
                'password' => $password,
                'wallet' => 0,
                'is_admin' =>  $isAdmin,
            ]);
        }
    }
}
