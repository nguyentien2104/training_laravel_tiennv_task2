<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'code',
        'order_status',
        'user_id',
        'ship_fee',
        'paid_status',
        'discount_code_id',
        'note',
    ];
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
