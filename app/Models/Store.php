<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'address',
        'area_id',
        'type',
        'time_start',
        'time_end',
    ];

    public function area()
    {
        return $this->belongsTo(Area::class,'area_id');
    }
}
