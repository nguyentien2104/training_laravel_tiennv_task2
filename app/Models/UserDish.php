<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDish extends Model
{
    use HasFactory;
    public $table = "user_dish";
    protected $fillable = [
        'order_id',
        'dish_id',
        'number_dish',
        'total_amount',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function user1()
    {
        return $this->belongsTo(Dish::class, 'dish_id');
    }
}
