<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    use HasFactory;
    public $table = "dishs";
    protected $fillable = [
        'id',
        'name',
        'price',
        'store_id',

    ];
    public function user_dish()
    {
        return $this->hasMany(User_Dish::class, 'dish_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class,'store_id');
    }
}
