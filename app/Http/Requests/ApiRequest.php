<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ApiRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = [
            'message' => 'Validation errors in your request',
            'errors' => $validator->errors()->toArray()
        ];
        throw new HttpResponseException(response()->json($errors, 422));
    }
}
