<?php

namespace App\Http\Requests\Area;

use Illuminate\Foundation\Http\FormRequest;

class CreateAreaRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:areas',
            'ship_fee' => 'required|integer',
        ];
    }
    public function getData()
    {
        return $this->only(['name', 'ship_fee']);
    }
}
