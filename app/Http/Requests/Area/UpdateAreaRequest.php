<?php

namespace App\Http\Requests\Area;

use App\Http\Requests\ApiRequest;
use App\Models\Area;

class UpdateAreaRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:areas,name,' . $this->id,
            'ship_fee' => 'required|integer',
        ];
    }
    public function getData()
    {
        return $this->only(['name', 'ship_fee']);
    }
}
