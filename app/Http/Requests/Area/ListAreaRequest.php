<?php

namespace App\Http\Requests\Area;

use App\Http\Requests\ApiRequest;

class ListAreaRequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'search_name' => 'string|max:255',
            'ship_fee_from' => 'integer',
            'ship_fee_to' => 'integer',
        ];
    }
}
