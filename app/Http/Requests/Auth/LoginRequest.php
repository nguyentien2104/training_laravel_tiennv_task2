<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;


class LoginRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ];
    }
    public function getData()
    {
        return $this->only(['email', 'password']);
    }
}
