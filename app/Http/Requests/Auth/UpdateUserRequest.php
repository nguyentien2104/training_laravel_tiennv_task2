<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;

class UpdateUserRequest extends ApiRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . auth()->id(),
            'phone' => 'required|max:20',
            'address' => 'required|max:255',
            'gender' => 'required|in:0,1',
        ];
    }
    public function getData()
    {
        return $this->only(['name', 'phone', 'address', 'gender', 'email']);
    }
}
