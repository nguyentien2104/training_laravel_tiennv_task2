<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\ApiRequest;

class RegisterRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|max:20|min:8|same:password_confirmation',
        ];
    }
    public function getData()
    {
        return $this->only(['name', 'phone', 'address', 'gender', 'email', 'password']);
    }
}
