<?php

namespace App\Http\Requests\User;

use App\Enums\UserRole;
use App\Http\Requests\ApiRequest;
use App\Models\User;

class UpdateUserRequest extends ApiRequest
{

    public function authorize()
    {
        $user = User::findOrFail($this->id);
        if ($user->is_admin == UserRole::ADMINISTRATOR) {
            return false;
        }
        return true;
    }


    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $this->id, //Để nó có thể update lại email hiện tại của nó mà ko bị check trùng
            'phone' => 'required|max:20',
            'gender' => 'required|in:0,1',
            'address' => 'required|string|max:255',
        ];
    }
    public function getData()
    {
        return $this->only(['name', 'phone', 'address', 'gender', 'email']);
    }
}
