<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;

class RechargeWalletrequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'amount'=> 'integer',
        ];
    }
    public function getData()
    {
        return $this->only('email','amount') ;
    }
}
