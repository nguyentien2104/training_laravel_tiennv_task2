<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;

class CreateUserRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|max:20',
            'gender' => 'required|in:0,1',
            'email' => 'required|email|max:255|unique:users',
        ];
    }
    public function getData()
    {
        return $this->only(['name', 'phone', 'address', 'gender', 'email',]);
    }
}
