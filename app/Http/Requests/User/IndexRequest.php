<?php

namespace App\Http\Requests\User;

use App\Http\Requests\ApiRequest;

class IndexRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'search_name' => 'string|max:255',
        ];
    }
}
