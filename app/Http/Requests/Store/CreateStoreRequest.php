<?php

namespace App\Http\Requests\Store;

use App\Http\Requests\ApiRequest;

class CreateStoreRequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'area_id' => 'required|integer|exists:areas,id',
            'type' => 'in:1,2',
            'time_start' => 'required|integer|min:1|max:24',
            'time_end' => 'required|integer|min:1|max:24',

        ];
    }
    public function getData()
    {
        return $this->only('name', 'address', 'area_id', 'type', 'time_start', 'time_end');
    }
}
