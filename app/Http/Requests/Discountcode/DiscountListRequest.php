<?php

namespace App\Http\Requests\Discountcode;

use App\Http\Requests\ApiRequest;

class DiscountListRequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'search_name' => 'string|max:255',
            'amount_from' => 'integer',
            'amount_to' => 'integer',
        ];
    }
}
