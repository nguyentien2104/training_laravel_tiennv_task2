<?php

namespace App\Http\Requests\Discountcode;

use App\Http\Requests\ApiRequest;


class CreateDiscountCodeRequest extends ApiRequest
{

    public function rules()
    {
        return [
            'code' => 'required|string|max:255|unique:discount_codes',
            'amount' => 'required|integer',
        ];
    }
    public function getData()
    {
        return $this->only('code', 'amount');
    }
}
