<?php

namespace App\Http\Requests\Discountcode;

use App\Http\Requests\ApiRequest;

class UpdateDiscountCodeRequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
            'code' => 'required|string|max:255|unique:discount_codes,code,' . $this->id,
            'amount' => 'required|integer',
        ];
    }

    public function getData()
    {
        return $this->only('code', 'amount');
    }
}
