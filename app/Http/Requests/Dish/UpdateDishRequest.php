<?php

namespace App\Http\Requests\Dish;

use App\Http\Requests\ApiRequest;

class UpdateDishRequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:dishs,name,' . $this->id,
            'price' => 'required|integer',
            'store_id' => 'required|integer|exists:stores,id',
        ];
    }

    public function getData()
    {
        return $this->only('name', 'price', 'store_id');
    }
}
