<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\ApiRequest;

class OrderRequest extends ApiRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        return [
           'dish_ids' =>'required|array',
           'dish_ids.*.id' =>'required|integer|exists:dishs,id',
           'dish_ids.*.number' =>'required|integer',
           'number_dish' => 'integer',
           'note' => 'string|max:255',
           'discount_code' => 'string|max:255|exists:discount_codes,code',
        ];
    }
}
