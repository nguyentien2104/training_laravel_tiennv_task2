<?php

namespace App\Http\Middleware;

use App\Enums\UserRole;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserAdmin
{
    public function handle(Request $request, Closure $next)
    {
        try {
            $jwt = JWTAuth::parseToken()->authenticate();
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            $jwt = false;
        }
        if (Auth::check() || $jwt) {
            if (Auth::user()->is_admin == 0) {
                return response(['message' => 'Bạn không có quyền admin.'], 401);
            }
            return $next($request);
        } else {
            return response(['message' => 'Unauthorized.'], 401);
        }
    }
}
