<?php

namespace App\Http\Middleware;

use App\Enums\UserRole;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserApiMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        try {
            $jwt = JWTAuth::parseToken()->authenticate();
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            $jwt = false;
        }
        if (Auth::check() || $jwt) {
            if (Auth::user()->is_admin == 1) {
                return response(['message' => 'Chỉ có người dùng được truy cập.'], 401);
            }
            return $next($request);
        } else {
            return response(['message' => 'Unauthorized.'], 401);
        }
    }
}
