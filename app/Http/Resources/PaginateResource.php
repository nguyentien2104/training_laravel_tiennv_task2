<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaginateResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'total' => $this->total(),
            'per_page' => $this->perPage(),
            'current_page' => $this->currentPage(),
            'last_page' => $this->lastPage(),
            'next' => $this->nextPageUrl() ? $this->currentPage() + 1 : null,
            'prev' => $this->previousPageUrl() ? $this->currentPage() - 1 : null,
        ];
    }
}
