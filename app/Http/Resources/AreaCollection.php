<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AreaCollection extends ResourceCollection
{

    public $collects = AreaResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
