<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'address' => $this->address,
            'area_name' => optional($this->area)->name,
            'type' => $this->type,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
        ];
    }
}
