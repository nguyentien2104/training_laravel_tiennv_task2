<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DiscountCodeColletion extends ResourceCollection
{
    public $collects = DiscountCodeResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
