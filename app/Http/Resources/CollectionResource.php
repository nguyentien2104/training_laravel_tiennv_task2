<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\PaginateResource;

class CollectionResource extends ResourceCollection
{
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function toResponse($request)
    {
        return JsonResource::toResponse($request);
    }

    public function with($request)
    {
        return [
            'paginate' => new PaginateResource($this)
        ];
    }
}
