<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DiscountCodeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'code' => $this->code,
            'amount' => $this->amount,
        ];
    }
}
