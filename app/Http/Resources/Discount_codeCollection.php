<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Discount_codeCollection extends ResourceCollection
{

    public $collects = Discount_codeResource::class;
    public function toArray($request)
    {
        return $this->collection;
    }
}
