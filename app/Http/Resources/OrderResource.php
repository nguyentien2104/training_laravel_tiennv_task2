<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'order_status' => $this->order_status,
            'user_id' => $this->user_id,
            'ship_fee' => $this->ship_fee,
            'paid_status' => $this->paid_status,
            'discount_code_id' => $this->discount_code_id
        ];
    }
}
