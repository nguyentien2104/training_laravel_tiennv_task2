<?php

namespace App\Http\Controllers;

class ApiController extends Controller
{

    protected function sendResponse($resource)
    {
        return $resource->response()->setStatusCode(200);
    }

    protected function sendError404($message)
    {
        return response()->json(['message' => $message], 404);
    }

    protected function formatJson($class_name, $item, $other = null)
    {
        return new $class_name($item, $other);
    }

    protected function formatCollectionJson($class_name, $item)
    {
        return $class_name::collection($item);
    }

    protected function sendMessage($message, $code = 200)
    {
        return response()->json(['message' => $message], $code);
    }

    protected function sendMessage422($message, $code = 422)
    {
        return response()->json(['message' => $message], $code);
    }

    protected function errorResponse($message, $code = 400)
    {
        return response()->json(['message' => $message], $code);
    }
    protected function formatCreate($message, $value, $code = 201)
    {
        $data = [
            "message" => $message,
            "data" => $value
        ];
        return response()->json($data, $code);
    }
    protected function formatUpdate($message, $value, $code = 201)
    {
        $data = [
            "message" => $message,
            "data" => $value
        ];
        return response()->json($data, $code);
    }
    protected function sendSuccessList($data)
    {
        return response()->json(['data' => $data], 200);
    }
    public function sendError403($message)
    {
        return response()->json(['message' => $message], 403);
    }
    public function successResponse($data, $code = 200)
    {
        return response()->json($data, $code);
    }
}
