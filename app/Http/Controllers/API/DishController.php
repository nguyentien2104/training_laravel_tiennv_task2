<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Dish\CreateDishRequest;
use App\Http\Requests\Dish\ListDishRequest;
use App\Http\Requests\Dish\UpdateDishRequest;
use App\Http\Resources\DishResource;
use App\Models\Dish;

class DishController extends ApiController
{

    public function list(ListDishRequest $request)
    {
        $searchName = $request->search_name;
        $nameStore = $request->name_store;
        $dishs = Dish::orderBy('id', 'DESC');
        if (!is_null($searchName)) {
            $dishs = $dishs->where('name', 'LIKE', '%' . $searchName . '%');
        }
        dd('nguyentiendish') ;
        if (!is_null($nameStore)) {
            $dishs = $dishs->whereHas('store', function ($q) use ($nameStore) {
                $q->where('name', 'LIKE', '%' . $nameStore . '%');
            });
        }
        return  $this->formatCollectionJson(DishResource::class, $dishs->paginate(config('setting.paginate')));
    }

    public function create(CreateDishRequest $request)
    {
        try {
            $data = $request->getData();
            $dish = Dish::create($data);
            return $this->formatJson(DishResource::class, $dish);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function detail($id)
    {
        $dish = Dish::findOrFail($id);
        return $this->formatJson(DishResource::class, $dish);
    }

    public function update(UpdateDishRequest $request, $id)
    {
        $dish = Dish::findOrFail($id);
        $dish->update($request->getData());
        return $this->formatJson(DishResource::class, $dish);
    }


    public function delete($id)
    {
        $dish = Dish::findOrFail($id);
        $dish->delete();
        return $this->sendMessage([trans('message.dish.success.delete')], 200);
    }

    public function listDishByStore(ListDishRequest $request )
    {

        $storeId = $request->store_id;
        $dishs = Dish::orderBy('id', 'DESC');
        if (!is_null($storeId)) {
            $dishs = $dishs->whereHas('store', function ($q) use ($storeId) {
                $q->where('store_id',$storeId);
            });
        }
        return  $this->formatCollectionJson(DishResource::class, $dishs->paginate(config('setting.paginate')));
    }
}
