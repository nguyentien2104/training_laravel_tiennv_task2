<?php

namespace App\Http\Controllers\API;

use App\Enums\Status;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Order\OrderRequest;
use App\Http\Resources\OrderResource;
use App\Models\Area;
use App\Models\DiscountCode;
use App\Models\Dish;
use App\Models\Order;
use App\Models\OrderHistory;
use App\Models\OrderStore;
use App\Models\Store;
use App\Models\Transaction;
use App\Models\UserDish;
use Carbon\Carbon;
use Illuminate\Support\Str;

class OrderController extends ApiController
{
    public function order(OrderRequest $request)
    {
        $user = auth('api')->user();
        $dishIds = array_column($request->dish_ids, 'id');
        $dishs = Dish::whereIn('id', $dishIds);
        $storeIds = $dishs->pluck('id')->toArray();
        $areaIds = array_unique(Store::whereIn('id', $storeIds)->pluck('area_id')->toArray());
        $shipFee = Area::whereIn('id', $areaIds)->sum('ship_fee');
        $arrayDishIdAmount = $dishs->pluck('price', 'id');
        $totalAmountDish = 0;
        $arrayStoreId = $dishs->pluck('store_id', 'id');
        foreach ($request->dish_ids as $value) {
            $totalAmountDish += $arrayDishIdAmount[$value['id']] * $value['number'];
        }
        $discountCode = DiscountCode::where('code', $request->discount_code);
        $discountCodeDate = $discountCode->where('created_at', '>=', Carbon::now()->subDays(30));
        if (!is_null($discountCodeDate)) {
            $totalAmount = $shipFee + $totalAmountDish - ($discountCodeDate->value('amount'));
        } else {
            $totalAmount = $shipFee +  $totalAmountDish;
        }
        $updateWallet = ($user->wallet) - $totalAmount;
        if ($user->wallet >= abs($totalAmount)) {
            $user->update(array('wallet' => $updateWallet));
            $paidStatus = TRUE;
        } else {
            $paidStatus = FALSE;
        }

        $order = Order::create([
            'code' => Str::random(8),
            'order_status' => Status::CREATE,
            'user_id' => $user->id,
            'ship_fee' => $shipFee,
            'paid_status' => $paidStatus,
            'discount_code_id' => $discountCode->value('id'),
            'note' => 'Đặt hàng thành công',
        ]);
        if ($paidStatus) {
            $transaction = Transaction::create([
                'code' =>  Str::random(8),
                'user_id' => $user->id,
                'order_id' => $order->id,
                'description' => 'Thanh toán cho đơn hàng ' . $order->code,
                'amount' =>   $totalAmount,
                'change' => FALSE,
            ]);
        }

        foreach ($request->get('dish_ids') as $value) {
            $totalAmountDish = $arrayDishIdAmount[$value['id']] * $value['number'];
            $userDish = UserDish::create([
                'order_id' => $order->id,
                'dish_id' => $value['id'],
                'number_dish' => $value['number'],
                'total_amount' => $totalAmountDish,
            ]);
            $orderStore = OrderStore::create([
                'order_id' => $order->id,
                'store_id' =>  $arrayStoreId[$value['id']],
            ]);
        }
        $orderHistory = OrderHistory::create([
            'order_id' => $order->id,
            'order_status' => Status::CREATE
        ]);
        return $this->formatJson(OrderResource::class, $order);
    }
}
