<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Area\CreateAreaRequest;
use App\Http\Requests\Area\ListAreaRequest;
use App\Http\Requests\Area\UpdateAreaRequest;
use App\Http\Resources\AreaResource;
use App\Models\Area;


class ManagementAreaController extends ApiController
{

    public function list(ListAreaRequest $request)
    {
        $searchName = $request->search_name;
        $shipFeeFrom = $request->ship_fee_from;
        $shipFeeTo = $request->ship_fee_to;
        $areas = Area::orderBy('id', 'DESC');
        if (!is_null($searchName)) {
            $areas = $areas->where('name', 'LIKE', '%' . $searchName . '%');
        }
        if (!is_null($shipFeeFrom)) {
            $areas = $areas->where('ship_fee', '>=', $shipFeeFrom);
        }
        if (!is_null($shipFeeTo)) {
            $areas = $areas->where('ship_fee', '<=', $shipFeeTo);
        }
        return $this->formatCollectionJson(AreaResource::class, $areas->paginate(config('setting.paginate')));
    }

    public function create(CreateAreaRequest $request)
    {
        try {
            $data = $request->getData();
            $area = Area::create($data);
            return $this->formatJson(AreaResource::class, $area);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function detail($id)
    {
        $area = Area::findOrFail($id);
        return $this->formatJson(AreaResource::class, $area);
    }

    public function update(UpdateAreaRequest $request, $id)
    {

        $area = Area::findOrFail($id);
        $area->update($request->getData());
        return $this->formatJson(AreaResource::class, $area);
    }

    public function delete($id)
    {
        $area = Area::findOrFail($id);
        $area->delete();
        return $this->sendMessage([trans('message.area.success.delete')], 200);
    }
}
