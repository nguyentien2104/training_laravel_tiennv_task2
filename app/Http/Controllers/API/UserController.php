<?php

namespace App\Http\Controllers\API;

use App\Enums\UserRole;
use App\Http\Controllers\ApiController;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\IndexRequest;
use App\Http\Requests\User\RechargeWalletrequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserCollection;
use App\Models\User;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Dish;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\User_Dish;

class UserController extends ApiController
{

    public function list(IndexRequest $request)
    {
        $searchName = $request->search_name;
        $users = User::orderBy('id', 'DESC');
        if (!is_null($searchName)) {
            $users = $users->where('name', 'LIKE', '%' . $searchName . '%');
        }
        return $this->formatJson(UserCollection::class, $users->paginate(config('setting.paginate')));
    }

    public function detail($id)
    {
        $user = User::findOrFail($id);
        return $this->formatJson(UserResource::class, $user);
    }

    public function create(CreateUserRequest $request)
    {
        try {
            $data = $request->getData();
            $data['password'] = Hash::make(config('setting.password_default'));
            $data['is_admin'] = UserRole::MEMBER;
            $data['wallet'] = 0;
            $user = User::create($data);
            return $this->formatJson(UserResource::class, $user);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->getData());
        return $this->formatJson(UserResource::class, $user);
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $this->sendMessage([trans('message.user.success.delete')], 200);
    }
    public function rechargeWallet(RechargeWalletrequest $request)
    {

        $data = $request->getData();
        $user = User::where('email', $data['email'])->first();
        if (!is_null($user)) {
            $data['wallet'] = (int)$user->wallet + (int)$data['amount'];
        }
        $user->update($data);

        return $this->formatJson(UserResource::class, $user);
    }






}
