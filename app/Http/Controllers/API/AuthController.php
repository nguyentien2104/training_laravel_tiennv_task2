<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\UpdateUserRequest;
use App\Models\User;
use App\Http\Resources\UserResource;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{

    public function login(LoginRequest $request)
    {
        try {
            $credentials['email'] = $request->input('email');
            $credentials['password'] = $request->input('password');
            $token = null;
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->sendMessage422(['message' => trans('message.user.unsuccess.login')], 422);
            }
            $user = auth()->user();
            $user = $user->setAttribute('token', $token);
            return $this->successResponse(['message' => trans('message.user.success.login'), 'data' => new UserResource($user)], 200);
        } catch (JWTException $e) {
            return $this->sendError403([trans('message.user.unsuccess.login')], 403);
        }
    }

    public function register(RegisterRequest $request)
    {
        try {
            $data = $request->getData();
            $data['password'] = Hash::make($data['password']);
            $data['is_admin'] = 0;
            $data['wallet'] = 0;
            $user = User::create($data);
            return $this->formatJson(UserResource::class, $user);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function logout()
    {
        auth()->logout();
        return $this->sendMessage(['message' => trans('message.user.success.logout')], 200);
    }

    public function getProfile()
    {
        $user = auth('api')->user();
        return $this->formatJson(UserResource::class, $user);
    }

    public function updateProfile(UpdateUserRequest $request)
    {
        $user = auth()->user();
        $data = $request->getData();
        $user->update($data);
        return $this->formatJson(UserResource::class, $user);
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 3600,
            'user' => auth()->user(),
        ]);
    }
}
