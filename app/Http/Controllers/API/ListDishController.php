<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\UserSite\ListDishRequest;

class ListDishController extends ApiController
{

    public function listDish(ListDishRequest $request)
    {
        $searchName = $request->search_name;
        $dishs = Dish::orderBy('id', 'DESC');
        if (!is_null($searchName)) {
            $dishs = $dishs->where('name', 'LIKE', '%' . $searchName . '%');
        }
        if (!is_null($nameStore)) {
            $dishs = $dishs->whereHas('store', function ($q) use ($nameStore) {
                $q->where('name', 'LIKE', '%' . $nameStore . '%');
            });
        }
        return  $this->formatCollectionJson(DishResource::class, $dishs->paginate(config('setting.paginate')));
    }
}
