<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Discountcode\CreateDiscountCodeRequest;
use App\Http\Requests\Discountcode\DiscountListRequest;
use App\Http\Requests\Discountcode\UpdateDiscountCodeRequest;
use App\Http\Resources\DiscountCodeResource;
use App\Models\DiscountCode;

class DiscountController extends ApiController
{

    public function list(DiscountListRequest $request)
    {
        $searchName = $request->search_name;
        $amountFrom = $request->amount_from;
        $amountTo = $request->amount_to;
        dd('tiennv1') ;
        $discountCodes = DiscountCode::orderBy('id', 'DESC');
        if (!is_null($searchName)) {
            $discountCodes = $discountCodes->where('code', 'LIKE', '%' . $searchName . '%');
        }
        if (!is_null($amountFrom)) {
            $discountCodes = $discountCodes->where('amount', '>=', $amountFrom);
        }
        dd('tiennv22223333') ;
        if (!is_null($amountTo)) {
            $discountCodes = $discountCodes->where('amount', '<=', $amountTo);
        }
        dd('tiennv3333333') ;
        return $this->formatCollectionJson(DiscountCodeResource::class, $discountCodes->paginate(config('setting.paginate')));
    }

    public function create(CreateDiscountCodeRequest $request)
    {
        try {
            $data = $request->getData();
            $discountCode = DiscountCode::create($data);
            return $this->formatJson(DiscountCodeResource::class, $discountCode);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function detail($id)
    {
        $discountCode = DiscountCode::findOrFail($id);
        return $this->formatJson(DiscountCodeResource::class, $discountCode);
    }

    public function update(UpdateDiscountCodeRequest $request, $id)
    {
        $discountCode = DiscountCode::findOrFail($id);
        $discountCode->update($request->getData());
        return $this->formatJson(DiscountCodeResource::class, $discountCode);
    }

    public function delete($id)
    {
        $discountCode = DiscountCode::findOrFail($id);
        $discountCode->delete();
        return $this->sendMessage([trans('message.discountCode.success.delete')], 200);
    }
}
