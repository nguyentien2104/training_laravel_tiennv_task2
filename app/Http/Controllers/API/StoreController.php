<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Store\CreateStoreRequest;
use App\Http\Requests\Store\ListStoreRequest;
use App\Http\Requests\Store\UpdateStoreRequest;
use App\Http\Resources\StoreResource;
use App\Models\Store;

class StoreController extends ApiController
{
    public function list(ListStoreRequest $request)
    {
        $searchName = $request->search_name;
        $nameArea = $request->name_area;
        $stores = Store::orderBy('id', 'DESC');
        if (!is_null($searchName)) {
            $stores = $stores->where('name', 'LIKE', '%' . $searchName . '%');
        }
        if (!is_null($nameArea)) {
            $stores = $stores->whereHas('area', function ($q) use ($nameArea) {
                $q->where('name', 'LIKE', '%' . $nameArea . '%');
            });
        }

        return  $this->formatCollectionJson(StoreResource::class, $stores->paginate(config('setting.paginate')));
    }

    public function create(CreateStoreRequest $request)
    {
        try {
            $data = $request->getData();
            $store = Store::create($data);
            return $this->formatJson(StoreResource::class, $store);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function detail($id)
    {
        $store = Store::findOrFail($id);
        return $this->formatJson(StoreResource::class, $store);
    }

    public function update(UpdateStoreRequest $request, $id)
    {
        $store = Store::findOrFail($id);
        $store->update($request->getData());
        return $this->formatJson(StoreResource::class, $store);
    }

    public function delete($id)
    {
        $store = Store::findOrFail($id);
        $store->delete();
        return $this->sendMessage([trans('message.store.success.delete')], 200);
    }
}
