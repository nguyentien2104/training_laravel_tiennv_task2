<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Status extends Enum
{
    const CREATE =   0;
    const TRANSPORT =   1;
    const RECEIVED = 2;
}
