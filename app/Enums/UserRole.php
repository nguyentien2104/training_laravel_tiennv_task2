<?php

namespace App\Enums;

use Illuminate\Validation\Rules\Enum;

final class UserRole extends Enum
{
    const MEMBER =   0;
    const ADMINISTRATOR =   1;
}
