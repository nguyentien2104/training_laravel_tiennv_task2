<?php

namespace App\Helpers;

use Carbon\Carbon;


class Helper
{


    public static function formatDate($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d/m/Y');
    }
    
    public static function formatFromDate($fromDate)
    {
        return date('Y-m-d', strtotime(\Carbon\Carbon::createFromFormat('d/m/Y', $fromDate)));
    }

    public static function formatToDate($toDate)
    {
        return date('Y-m-d', strtotime(\Carbon\Carbon::createFromFormat('d/m/Y', $toDate)) + 60 * 60 * 24);
    }
    public static function setSelected($val, $origin)
    {
        $active = $val == $origin ? 'selected' : '';
        return $active;
    }

    public static function formatSqlDate($date)
    {
        if (is_null($date)) {
            return null;
        }
        $dt = new Carbon($date);
        return $dt->format('Y-m-d');
    }
    public static function indexPaginate($object, $loop)
    {
        $index = ($object->currentPage() - 1) * $object->perPage() + $loop->iteration;
        return $index;
    }

    public static function setChecked($val, $origin)
    {
        $active = $val == $origin ? 'checked' : '';
        return $active;
    }

    public static function formatDateChat($date)
    {
        $dt = is_null($date) ? Carbon::now() : new Carbon($date);
        return  $dt->format('d-m-Y H:i');
    }
    public static function formatDateNew($date)
    {
        if (is_null($date)) {
            return null;
        } else {
            $dt = new Carbon($date);
            return  $dt->format('d-m-Y');
        }
    }
    public function sendMessage($message, $code = 200)
    {
        $data = [
            "message" => $message,
        ];
        return response()->json($data, $code);
    }

}


